import argparse
import base64
import httplib2
import json
 
from pprint import pprint
 
from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
 
DISCOVERY_URL='https://{api}.googleapis.com/$discovery/rest?version={apiVersion}'
 
def main(photo_file):
    """Run a label request on a single image"""
 
    credentials = GoogleCredentials.get_application_default()
    service = discovery.build('vision', 'v1', credentials=credentials,
                              discoveryServiceUrl=DISCOVERY_URL)
 
    with open(photo_file, 'rb') as image:
        image_content = base64.b64encode(image.read())
        service_request = service.images().annotate(body={
            'requests': [{
                'image': {
                    'content': image_content.decode('UTF-8')
                },
                'features': [{
                    'type': 'FACE_DETECTION',
                    'maxResults': 1
                }]
            }]
        })
 
        response = service_request.execute()
        #pprint(response)
        data = response["responses"]
	data = data[0]
	data = data["faceAnnotations"]
	data = data[0]
	emotion = data["joyLikelihood"]
	if emotion == "VERY_LIKELY":
		e = 5
	elif emotion == "LIKELY":
		e = 4
	elif emotion == "UNLIKELY":
		e = 2
	elif emotion == "VERY_UNLIKELY":
		e = 1
	else:
		e = 3
	print e
	direction = data["panAngle"]
	if -10 <= direction <= 10:
		m = "B"
	elif -40 <= direction <= -20:
		m = "A"
	elif 20 <= direction <= 40:
		m = "C"
	elif -100 <= direction <= -80:
		m = "D"
	elif 80 <= direction <= 100:
		m = "E"
	else: 
		print "other"
	print m
	return 0
 
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('image_file', help='The image you\'d like to label.')
    args = parser.parse_args()
    main(args.image_file)
