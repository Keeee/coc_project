import time
import picamera

camera = picamera.PiCamera()

camera.start_preview()
time.sleep(5)
camera.capture('cameratest.jpg')
camera.stop_preview()
camera.close()
